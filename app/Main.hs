module Main where

import Spooky.Server.App (serverMain)

main :: IO ()
main = serverMain
