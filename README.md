# What's this?

Creepy Spock server

# How to start?

```bash
stack build --exec spooky
```

# How to devel?

Plain stack:

```bash
stack build --fast --file-watch
```

GHCId:

```bash
ghcid
```

# WTF?

## MTV

Project structure follows the [Model-Template-View] layout from Django framework:

```
src
└── Spooky                   # The Project
    ├── App                  # Site sections AKA subsites AKA apps
    │   ├── Routes.hs        # Router root - middleware and subcomponents
    │   ├── Site             # Basic site pages - index, abouts, etc.
    │   │   ├── Routes.hs    # /, /about, etc. Keep to a minimum!
    │   │   ├── Models.hs
    │   │   ├── Templates.hs
    │   │   └── Views.hs
    │   └── SomeApp          # Site application, usually mounted at "/some"
    │       ├── Routes.hs    # URLs relative to "/some"
    │       ├── Models.hs    # Data types and where to find them
    │       ├── Templates.hs # How to display data found in view
    │       └── Views.hs     # Find data and stuff into template
    ├── Server
    │   ├── App.hs           # Assemble and serve a site
    │   ├── Auth.hs          # Auth context helpers
    │   └── Types.hs         # Site-wide types like Db, Session, Environment
    └── Template
        ├── Layouts.hs       # Site-wide layouts and helpers
        └── Page.hs          # Tools to assemble a page
```

[Model-Template-View]: https://docs.djangoproject.com/en/1.10/faq/general/#django-appears-to-be-a-mvc-framework-but-you-call-the-controller-the-view-and-the-view-the-template-how-come-you-don-t-use-the-standard-names
