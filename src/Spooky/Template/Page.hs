module Spooky.Template.Page
  ( Page(..)
  , Placement(..)
  , emptyPage
  , renderPage
  , extendPage
  ) where

import Lucid
import Data.Text (Text, intercalate)

data Placement
  = Link
  | Tag
  deriving (Show)

data Page = Page
  { pageTitle     :: [Text]
  , pageHeadFirst :: Html ()
  , pageStyles    :: [(Placement, Text)]
  , pageHeadLast  :: Html ()
  , pageBody      :: Html ()
  , pageScripts   :: [(Placement, Text)]
  } deriving (Show)

instance Monoid Page where
  mempty = emptyPage
  mappend = extendPage

emptyPage :: Page
emptyPage = Page
  { pageTitle     = []
  , pageHeadFirst = pure ()
  , pageStyles    = []
  , pageHeadLast  = pure ()
  , pageBody      = pure ()
  , pageScripts   = []
  }

renderPage :: Text -> Page -> Html ()
renderPage titleSep Page{..} = doctypehtml_ (head_ mkHead >> body_ mkBody)
    where
      mkHead :: Html ()
      mkHead = do
        meta_ [charset_ "utf-8"]
        title_ . toHtml . intercalate titleSep $ reverse pageTitle
        pageHeadFirst
        sequence_
          [ link_
              [ rel_ "stylesheet"
              , href_ url
              ]
          | (Link, url) <- pageStyles
          ]
        sequence_
          [ style_ body
          | (Tag, body) <- pageStyles
          ]
        pageHeadLast

      mkBody :: Html ()
      mkBody = do
        pageBody
        sequence_
          [ script_ [ src_ url ] ("" :: Text)
          | (Link, url) <- pageScripts
          ]
        sequence_
          [ script_ body
          | (Tag, body) <- pageScripts
          ]

extendPage :: Page -> Page -> Page
extendPage a b = Page
    { pageTitle     = pageTitle     a `mappend` pageTitle     b
    , pageHeadFirst = pageHeadFirst a `mappend` pageHeadFirst b
    , pageStyles    = pageStyles    a `mappend` pageStyles    b
    , pageHeadLast  = pageHeadLast  a `mappend` pageHeadLast  b
    , pageBody      = pageBody      a `mappend` pageBody      b
    , pageScripts   = pageScripts   a `mappend` pageScripts   b
    }
