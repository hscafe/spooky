module Spooky.Template.Layouts
  ( HtmlAction, htmlPage
  , defaultLayout
  , bootstrapLayout
  , bootstrapV4
  , module Page
  ) where

import Control.Monad.Trans (MonadIO)
import Lucid
import Web.Spock (lazyBytes, setHeader, ActionCtxT)

import Spooky.Template.Page as Page

type HtmlAction = forall ctx m. MonadIO m => ActionCtxT ctx m ()

htmlPage :: Page -> HtmlAction
htmlPage p = do
  setHeader "Content-Type" "text/html; charset=utf-8"
  lazyBytes . renderBS $ Page.renderPage " :: " p

defaultLayout :: Html () -> Page
defaultLayout pageBody = bootstrapLayout $ do
  siteHeader
  siteBody
  siteFooter
  where
    siteHeader =
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-sm-4" ] $
          img_ [ class_ "img-fluid", src_ "/img/generic-logo-hi.png" ]
        div_ [ class_ "col-sm-8" ] $
          div_ [ class_ "display-3" ] "site header"

    siteBody = div_ $ do
      h1_ "page body"
      pageBody

    siteFooter = div_ "site footer"

bootstrapLayout :: Html () -> Page
bootstrapLayout body = mconcat
  [ bootstrapV4
  , emptyPage { pageBody = bootstrapBody }
  ]
  where
    bootstrapBody =
      div_ [class_ "container"] body

bootstrapV4 :: Page
bootstrapV4 = emptyPage
  { pageStyles =
      [ (Link, "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css")
      ]
  , pageScripts =
      [ (Link, "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js")
      ]
  }
