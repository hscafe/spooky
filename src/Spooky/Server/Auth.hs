module Spooky.Server.Auth
  ( UserId
  , User(..)
  , IsGuest(..)
  , IsAdmin(..)
  , guestOnly
  , requireUser
  , requireAdmin
  , maybeUser
  ) where

import Data.HVect
import Web.Spock

import Spooky.Server.Types

type UserId = Int
data User = User

data IsGuest = IsGuest
data IsAdmin = IsAdmin

guestOnly :: ActionM (HVect xs) (HVect (IsGuest ': xs))
guestOnly = maybeUser $ \case
  Just _ ->
    redirect "/"
  Nothing -> do
    oldCtx <- getContext
    pure (IsGuest :&: oldCtx)

requireUser :: ActionM (HVect xs) (HVect ((UserId, User) ': xs))
requireUser = maybeUser $ \case
  Nothing ->
    redirect "/auth/login"
  Just user -> do
    oldCtx <- getContext
    pure (user :&: oldCtx)

requireAdmin
  :: ListContains n (UserId, User) xs
  => ActionM (HVect xs) (HVect (IsAdmin ': xs))
requireAdmin = do
  oldCtx <- getContext
  if userIsAdmin (findFirst oldCtx)
    then pure (IsAdmin :&: oldCtx)
    else text "Go away!"

maybeUser
  :: (Maybe (UserId, User) -> ActionM ctx a)
  -> ActionM ctx a
maybeUser action = do
  Sess <- readSession
  action Nothing

userIsAdmin :: (UserId, User) -> Bool
userIsAdmin = \case
  (0, _) -> True
  _      -> False
