module Spooky.Server.App
  ( serverMain
  , initEnv
  ) where

import Web.Spock
import Web.Spock.Config

import Spooky.App.Routes (appRoot)
import Spooky.Server.Types (Env(Env), emptySess, Db)

serverMain :: IO ()
serverMain = do
  env <- initEnv
  db <- initDb
  cfg <- defaultSpockCfg emptySess db env
  runSpock 8080 $ spock
    cfg { spc_csrfProtection = True }
    appRoot

initEnv :: IO Env
initEnv = pure Env

initDb :: IO (PoolOrConn Db)
initDb = pure PCNoDatabase
