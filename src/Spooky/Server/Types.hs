module Spooky.Server.Types
  ( Env(..)
  , Sess(..)
  , emptySess
  , Db
  , ActionM
  , RouteM
  , RouteCtxM
  ) where

import Web.Spock

data Env = Env

data Sess = Sess

emptySess :: Sess
emptySess = Sess

type Db = ()

type ActionM ctx a = SpockActionCtx ctx Db Sess Env a

type RouteM = SpockM Db Sess Env ()

type RouteCtxM ctx = SpockCtxM ctx Db Sess Env ()
