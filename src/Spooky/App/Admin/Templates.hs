module Spooky.App.Admin.Templates
  ( adminRoot
  ) where

import Lucid

import Spooky.Template.Layouts

adminRoot :: HtmlAction
adminRoot = htmlPage . defaultLayout $ do
  div_ "admin root, but not really"
