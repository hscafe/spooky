module Spooky.App.Admin.Routes
  ( adminComponent
  ) where

import           Data.HVect
import           Web.Spock

import Spooky.Server.Types (RouteCtxM)
import Spooky.Server.Auth (UserId, User, requireAdmin)
import Spooky.App.Admin.Views (adminRoot, adminTest)

adminComponent :: ListContains n (UserId, User) xs => RouteCtxM (HVect xs)
adminComponent = prehook requireAdmin $ do
  get root adminRoot
  get var adminTest
