module Spooky.App.Admin.Views
  ( adminRoot
  , adminTest
  ) where

import           Control.Monad.Trans (liftIO)
import           Data.HVect
import qualified Data.Text as T
import           Web.Spock

import           Spooky.Server.Types (ActionM)
import           Spooky.Server.Auth (UserId, User, IsAdmin(..))
import qualified Spooky.App.Admin.Templates as Render

type AdminAction ctx = forall n m.
  ( ListContains n IsAdmin ctx
  , ListContains m (UserId, User) ctx
  ) => ActionM (HVect ctx) ()

adminRoot :: AdminAction ctx
adminRoot = do
  IsAdmin <- fmap findFirst getContext
  Render.adminRoot

adminTest :: T.Text -> AdminAction ctx
adminTest v = do
  (_ :: (UserId, User)) <- fmap findFirst getContext
  liftIO $ print v
  text v
