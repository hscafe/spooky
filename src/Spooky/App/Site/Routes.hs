module Spooky.App.Site.Routes
  ( siteRoutes
  ) where

import           Web.Spock

import Spooky.Server.Types (RouteCtxM)
import Spooky.App.Site.Views (siteRoot)

siteRoutes :: RouteCtxM ctx
siteRoutes = do
  get root siteRoot
