module Spooky.App.Site.Templates
  ( siteRoot
  ) where

import Data.Text (Text)
import Lucid

import Spooky.Template.Layouts

siteRoot :: Text -> HtmlAction
siteRoot hello = htmlPage . defaultLayout $ do
  div_ $ toHtml hello
