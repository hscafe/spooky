module Spooky.App.Site.Views
  ( siteRoot
  ) where

import Web.Spock

import           Spooky.Server.Types (Env(..), ActionM)
import qualified Spooky.App.Site.Templates as Render

siteRoot :: ActionM ctx ()
siteRoot = do
  Env <- getState
  Render.siteRoot "hi there"
