module Spooky.App.Routes
  ( appRoot
  ) where

import           Data.HVect (HVect(HNil))
import           Network.Wai.Middleware.Static (staticPolicy, addBase)
import           Web.Spock

import Spooky.Server.Auth (requireUser)
import Spooky.Server.Types (ActionM, RouteM)
import Spooky.App.Site.Routes (siteRoutes)
import Spooky.App.Admin.Routes (adminComponent)

appRoot :: RouteM
appRoot = prehook initCtx $ do
  middleware (staticPolicy (addBase "static")) -- XXX: static files are injected into root!
  siteRoutes
  prehook requireUser $ do
    subcomponent "admin" adminComponent

initCtx :: ActionM () (HVect '[])
initCtx = pure HNil
